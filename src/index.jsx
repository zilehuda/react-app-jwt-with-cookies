import React from 'react';
import { render } from 'react-dom';
import { CookiesProvider } from "react-cookie";

import { App } from './App';

// setup fake backend
import { configureFakeBackend } from './_helpers';
configureFakeBackend();

render(
    <App />,
    document.getElementById('app')
);