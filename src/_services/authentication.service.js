import { BehaviorSubject } from "rxjs";

import config from "config";
import { handleResponse } from "@/_helpers";
import Cookies from "universal-cookie";
const cookies = new Cookies();

function getCurrentUser() {
  const data = cookies.get("data");
  if (data) {
    return data.user;
  }
  return undefined;
}
const currentUserSubject = new BehaviorSubject(
  // JSON.parse(localStorage.getItem("currentUser"))
  getCurrentUser()
);

export const authenticationService = {
  login,
  logout,
  currentUser: currentUserSubject.asObservable(),
  get currentUserValue() {
    return getCurrentUser();
  },
};

function login(username, password) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ email: username, password }),
  };
  return fetch(`${config.authUrl}/login/?is_company=true`, requestOptions)
    .then(handleResponse)
    .then((res) => {
      cookies.set("data", res.data, { path: "/", domain: config.appDomain });
      currentUserSubject.next(res);

      return res;
    });
}

function logout() {
  // remove user from local storage to log user out
  cookies.remove("data", { domain: config.appDomain });
  currentUserSubject.next(null);
}
